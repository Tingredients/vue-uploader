export default {
    name: 'DropzoneSetting',
    data() {
        return {
            dropzoneOptions: {
                url: `api.local.test/entries/upload`,
                createImageThumbnails: false,
                addRemoveLinks: false,
                uploadMultiple: false,
                maxFilesize: 10,
                accept: function(file, done) {
                    let allowed_type = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];

                    let type = file.type.split('/');

                    //Check allowed file extension
                    if ( !allowed_type.includes(type[1]) )  {
                        return done('Invalid file format. Please upload a JPG, JPEG, GIF, PNG or PDF');
                    } else {
                        return done();
                    }
                },
                dictDefaultMessage: "<i>UPLOAD RECEIPT</i>",
                dictFileTooBig: "Max file size must be less than 10MB",
                dictRemoveFile: "",
                previewTemplate: `<div class="dz-preview">
                                    <div class="dz-filename"><span data-dz-name></span></div>
                                    <a class="dz-remove" data-dz-remove ></a>
                                  </div>`
            },
            uploadPercentage: 0,
            showProgressBar: false
        }
    },
    methods: {
        getFileType(file) {
            let type = file.type.split('/');
            return type[1];
        }
    }
}